@extends('layout')

@section('menu')
    <p><a href="{{ url('') }}">Главная</a></p>
@endsection

@section('content')
    <h2>Публикационный рейтинг научно-педагогических работников</h2>

    <div class="publs">
        <table class="table table-striped" style="text-align: center;">
            <thead>
            <tr>
                <th scope="col">№</th>
                <th scope="col">Название документа</th>
                <th scope="col">Авторы</th>
                <th scope="col">Год</th>
                <th scope="col">Источник</th>
                <th scope="col">Цитирования</th>
            </tr>
            </thead>
            <tbody>
            <? $i=1;?>
            @foreach($results as $result)
                <tr>
                    <td style="width: 20px;">{{$i}}</td>
                    <td style="width: 20px;">
                        <a href="{{$result->publ_url}}" target="_blank">{{$result->title}}</a></td>
                    <td style="width: 20px;">{{$result->authors}}</td>
                    <td style="width: 20px;">{{$result->year}}</td>
                    <td style="width: 20px;">
                    <a href="{{$result->source_url}}" target="_blank">{{$result-> source_name}}</a></td>
                    <td style="width: 20px;">{{$result->citations}}</td>
                </tr>
                <? $i+=1;?>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection