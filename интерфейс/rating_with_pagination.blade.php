@extends('layout')

@section('menu')
    <p><a href="{{ url('') }}">Показать таблицу<br>с пагинацией    </a></p>
    <p><a href="{{ action('RatingController@create_table_without_pagination', []) }}" >Показать таблицу<br>полностью</a></p>
@endsection

@section('content')

    <h2 >Публикационный рейтинг научно-педагогических работников</h2>
    <div class="container" >
    {{--    <form action="{{ action('ExController@res2', []) }}" method="post">--}}
    {{--    <input type="checkbox" name="show_all" id="show_all" value="">Показать все<br>--}}
    {{--    </form>--}}
        @foreach($results2 as $res)
        <h6>Данные от {{$res->date_of_publication_on_site}}</h6>
        @endforeach
{{--        <div class="check">--}}
{{--            <p><a href="{{ url('') }}">Главная</a></p>--}}
{{--        <a href="{{ action('ExController@res2', []) }}" >Показать таблицу полностью</a>--}}
{{--        </div>--}}

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">№</th>
                <th scope="col">ФИО</th>
                <th scope="col">Ученое<br>звание</th>
                <th scope="col">Ученая степень</th>
                <th scope="col">Кол-во<br>публ. в Top10%</th>
                <th scope="col">Кол-во<br>публ. в Top25%</th>
                <th scope="col">Кол-во публ. в ост. изд.<br>(SNIP>0)</th>
                <th scope="col">Кол-во цитирований</th>
                <th scope="col">Рейтинг</th>
            </tr>
            </thead>
            <tbody>
            @foreach($results as $result)
                <tr>
                    <td style="width: 20px;">{{$i}}</td>
                    <td style="width: 20px;">
                        @if($result->id_scopus!=0)
                            <a href="https://www.scopus.com/authid/detail.uri?authorId={{$result->id_scopus}}" target="_blank">{{$result->last_name}}<br>{{$result->name}} {{$result->middle_name}}</a></td>
                        @else
                            {{$result->last_name}}<br>{{$result->name}} {{$result->middle_name}}</td>
                        @endif

                                           <td style="width: 20px;">{{$result->academic_title}}</td>
                    <td style="width: 20px;">{{$result->academic_degree}}</td>
                    <td style="width: 20px;">@if($result->N_top10!=0)
                        <a href="{{ action('RatingController@create_table_publs_top10', [$result->id, $result->last_name, $result->name, $result->middle_name]) }}" target="_blank">{{$result->N_top10}}</a>
                        @else
                        {{$result->N_top10}}
                        @endif
                    </td>
                    <td style="width: 20px;">@if($result->N_top25!=0)
                        <a href="{{ action('RatingController@create_table_publs_top25', [$result->id, $result->last_name, $result->name, $result->middle_name]) }}" target="_blank">{{$result->N_top25}}</a>
                        @else
                        {{$result->N_top25}}
                        @endif
                    </td>
                    <td style="width: 20px;">@if($result->N_other!=0)
                        <a href="{{ action('RatingController@create_table_publs_others', [$result->id, $result->last_name, $result->name, $result->middle_name]) }}" target="_blank">{{$result->N_other}}</a>
                        @else
                        {{$result->N_other}}
                        @endif
                    </td>
                    <td style="width: 20px;">{{$result->N_citations}}</td>
                    <td style="width: 20px;">{{$result->rating}}</td>
                </tr>
                <? $i+=1;?>
            @endforeach
            </tbody>
        </table>



        {!! $results->appends(Illuminate\Support\Facades\Input::except('ex'))->links() !!}

    @foreach($results3 as $res3)
    <div class="paint" style="margin-bottom: 15px;">
        Публикационный рейтинг – это интеграционный показатель, учитывающий количество публикаций и цитирований
        научных статей сотрудника в библиографической базе данных Scopus. В расчет принимаются
        только те публикации, которые вышли в изданиях с ненулевым SNIP. Рейтинг вычисляется
        по следующей формуле:
    </div>
                <div class="inp">
                    <input id="coef1" name="coef1" type="text" readonly value="{{$res3->coef_10}}" style="margin-left: -70px; width: 40px; font-size:18px;">
                    <input id="coef2" name="coef2" type="text" readonly value="{{$res3->coef_25}}" style="margin-left: 140px; width: 30px; font-size:18px;">
                    <input id="coef3" name="coef3" type="text" readonly value="{{$res3->coef_other}}" style="margin-left: 130px; width: 38px; font-size:18px;">
                    <input type="text" id="coef4" name="coef4" readonly value="{{$res3->coef_c}}" style="margin-left: 190px; width: 30px; font-size:18px;">
                </div>
            <div class="paint">
        где:<br>
        <b>NTop10%</b>  — количество статей НПР в журналах и изданиях Top-10% (за последние
                <?php $time="" ?>
                @if($res3->time_window%10==1)
                    <?php $time="год"?>
                @elseif($res3->time_window%10==2 or $res3->time_window%10==4 or $res3->time_window%10==3)
                    <?php $time="года"?>
                @else
                    <?php $time="лет"?>
                @endif
                <input id="window_time" name="window_time" type="text" readonly value="{{$res3->time_window}}"> {{$time}}),<br>
        <b>NTop25%</b>  — количество статей НПР в журналах и изданиях Top-25%, не входящих в Top-10% (за последние <input id="window_time" name="window_time" type="text" readonly value="{{$res3->time_window}}" > {{$time}}),<br>

                <input id="window_time" name="window_time" type="text" readonly value="{{$res3->time_window}}"> {{$time}}),<br>
        <b>NOther SNIP > 0</b>  — количество статей НПР в журналах и изданиях с ненулевым SNIP, не входящих в Top-10% и в Top-25% (за последние <input id="window_time" name="window_time" type="text" readonly value="{{$res3->time_window}}"> {{$time}}),<br>
        <b>CScopus</b>  — количество цитирований статей НПР в Scopus (за последние <input id="window_time" name="window_time" type="text" readonly value="{{$res3->time_window}}">  {{$time}}).
            </div>
        @endforeach
    </div>
@endsection
