<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Tickets;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input as Input;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Charts;

class RatingController extends Controller
{
    public function create_table_with_pagination($count=5)
    {
        $results = DB::select('call view_rating()');

        $results2 = DB::select('call find_date_of_create_rat()');

        $results3=DB::select('call show_coefs()');

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $col = new Collection($results);
        $perPage = $count;
        $i=($currentPage - 1) * $perPage+1;
        $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $entries = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);


        return view('rating_with_pagination', [
            'results' => $entries,
            'results2' => $results2,
            'results3' => $results3,
            'i'=>$i,
        ]);

    }

    public function create_table_without_pagination()
    {
        $results = DB::select('call view_rating()');
        $results2 = DB::select('call find_date_of_create_rat()');
        $results3=DB::select('call show_coefs()');

        return view('rating_without_pagination', [
            'results' => $results,
            'results2' => $results2,
            'results3' => $results3,
        ]);
    }


    public function create_table_publs_top10($id_scientist, $l_name, $name, $m_name)
    {
        $results = DB::select('call publications_top10_of_scientist(?)',[$id_scientist]);
        $str='Публикации из Top10%:';

        return view('publs_of_scientist', [
            'results' => $results,
            'l_name' => $l_name,
            'name' => $name,
            'm_name' => $m_name,
            'str' => $str,
        ]);
    }

    public function create_table_publs_top25($id_scientist, $l_name, $name, $m_name)
    {
        $results = DB::select('call publications_top25_of_scientist(?)',[$id_scientist]);
        $str='Публикации из Top25%:';

        return view('publs_of_scientist', [
            'results' => $results,
            'l_name' => $l_name,
            'name' => $name,
            'm_name' => $m_name,
            'str' => $str,
        ]);
    }

    public function create_table_publs_others($id_scientist, $l_name, $name, $m_name)
    {
        $results = DB::select('call publications_others_of_scientist(?)',[$id_scientist]);

        $str='Публикации из изданий, не входящих в Top10% и Top25%, но имеющих показатель Snip > 0:';

        return view('publs_of_scientist', [
            'results' => $results,
            'l_name' => $l_name,
            'name' => $name,
            'm_name' => $m_name,
            'str' => $str,
        ]);
    }
}
