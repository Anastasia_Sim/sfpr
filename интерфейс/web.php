<?php
//Маршрутизация между страницами интерфейса

Route::get('/', 'RatingController@create_table_with_pagination');

Route::get('/show_all', [ 'as' => 'create_table_without_pagination', 'uses' => 'RatingController@create_table_without_pagination' ]);

Route::get('/publications_of_scientist/{id_scientist}/{l_name}/{name}/{m_name}', [ 'as' => 'create_table_publs_top10', 'uses' => 'RatingController@create_table_publs_top10' ] );

Route::get('/publications_of_scientist2/{id_scientist}/{l_name}/{name}/{m_name}', [ 'as' => 'create_table_publs_top25', 'uses' => 'RatingController@create_table_publs_top25' ] );

Route::get('/publications_of_scientist3/{id_scientist}/{l_name}/{name}/{m_name}', [ 'as' => 'create_table_publs_others', 'uses' => 'RatingController@create_table_publs_others' ] );