"""Модуль формирования публикационного рейтинга научно-педагогических работников.
    Параметры запуска: : путь до файла со списком научных работников в формате .csv, временное окно, за которое будет 
    высчитываться рейтинг, коэффициенты для формулы расчета рейтинга.
    Версия интерпретатора: Python 3.7.2 64bit
    URL репозитория для скачивания: https://www.python.org/downloads/release/python-370/
    © 2019. Симонова А. 89080567599 simonova_anastasia97@mail.ru"""



import datetime
import MySQLdb
import csv
import pandas as pd
import scopus
from scopus import AuthorSearch
from scopus import AuthorRetrieval
from scopus import AffiliationSearch
from scopus import CitationOverview
from scopus import ScopusSearch
from scopus import ScopusAbstract
from scopus import ScopusJournal
from scopus import Search
from scopus import AbstractRetrieval
import requests
import pymysql
import unicodedata as unicode
import sys
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

from pyscopus import Scopus
key = '1f0560237798339e0131cfbab1637988'
scopus1 = Scopus(key)


#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8

import scopus
from scopus import AuthorSearch

class Scientist:
    def __init__(self, row):
        self.name = row[0]
        self.mname = row[1]
        self.lname = row[2]
        self.ac_title = row[3]
        self.ac_degree = row[4]
        self.en_initials = row[5]
        self.en_lname = row[6]
        search_author = AuthorSearch(
            "AUTHLASTNAME(" + self.en_lname + ") and AUTHFIRST(" + self.en_initials + ") "
                    "and AFFIL(South Ural State University)")
        inf = search_author.authors
        self.id_scopus = None
        if inf!=None:
            for value in inf:
                if value.affiliation == "South Ural State University" \
                        and value.city == "Chelyabinsk":
                    self.id_scopus = int(((value[0]).split('-'))[2])  # Получ ID

class Publication:

    def __init__(self, val):
        self.code=val
        ab = ScopusAbstract(val)
        self.title = ab.title
        self.url_scopus = ab.scopus_url
        self.year=int((ab.coverDate).split('-')[0])
        self.authors = ''
        for value in ab.authors:
            self.authors += value.indexed_name + ', '
        self.authors =  self.authors[:-2]
        self.citations = ab.citedby_count


class Source:

    def __init__(self, val):
        ab = ScopusAbstract(val)
        self.name = ab.publicationName
        self.id_scopus = ab.source_id
        self.url_scopus = "https://www.scopus.com/sourceid/"+str(self.id_scopus)

class ScopusDataCollection:

    def __init__(self, fname="", win_time=5, coef10=100, coef25=50, coef_oth=5, coef_count_citations=1):
        self.window_time=win_time
        self.coef_top10=coef10
        self.coef_top25 = coef25
        self.coef_others = coef_oth
        self.coef_C=coef_count_citations
        self.filename = fname

    def CheckTableOnExist(self, table_name, cursor):
        cursor.execute("SHOW TABLES")
        for x in cursor:
            if x[0] == table_name:
                return True
        return False

    def FillDB(self):

        db = MySQLdb.connect("localhost", "root", "", "")
        cursor = db.cursor()
        cursor.execute("SHOW DATABASES")
        is_exist=False
        for x in cursor:
            if x[0] == "rating":
                is_exist = True
                break
        if (is_exist == False):
            cursor.execute("create database rating")

        db = MySQLdb.connect("localhost", "root", "", "rating", charset='utf8')
        cursor = db.cursor()

        scientists=[]
        if (self.filename == "" and self.CheckTableOnExist('scientists', cursor)==False):
            raise Exception("Не указан файл и таблица не создана!")

        if (is_exist==False):
            cursor2 = db.cursor()
            cursor2.execute(
                "CREATE TABLE parameters (id INT AUTO_INCREMENT PRIMARY KEY, coef_10 INT, coef_25 INT, coef_other INT, coef_c INT, time_window INT)")
            db.commit()
            sql = "INSERT INTO parameters (coef_10, coef_25, coef_other, coef_c, time_window) VALUES (%s, %s, %s, %s, %s)"
            val = (self.coef_top10, self.coef_top25, self.coef_others,  self.coef_C, self.window_time)
            cursor2.execute(sql, val)
            db.commit()
            d = datetime.date.today()
            q=0
            if(d.month<=3):
                q=1
            elif(d.month>3 and d.month<=6):
                q=2
            elif(d.month>6 and d.month<=9):
                q=3
            else:
                q=4
            cursor2.execute(
                "CREATE TABLE editions (id INT AUTO_INCREMENT PRIMARY KEY, year INT, quarter INT, date_of_publication_on_site DATE, id_coef_of_formula INT,  FOREIGN KEY(id_coef_of_formula) REFERENCES parameters(id))")
            db.commit()

            sql = "INSERT INTO editions (year, quarter, date_of_publication_on_site, id_coef_of_formula) VALUES (%s, %s, %s, %s)"
            val = (d.year, q, d, 1)
            cursor2.execute(sql, val)
            db.commit()

        if (is_exist == True):
            cursor2 = db.cursor()
            sql = "INSERT INTO parameters (coef_10, coef_25, coef_other, coef_c, time_window) VALUES (%s, %s, %s, %s, %s)"
            val = (self.coef_top10, self.coef_top25, self.coef_others,  self.coef_C, self.window_time)
            cursor2.execute(sql, val)
            db.commit()
            cursor.execute("SELECT * FROM parameters ORDER BY id DESC LIMIT 1")
            res1 = cursor.fetchall()
            id=res1[0][0]

            cursor.execute("SELECT * FROM editions ORDER BY id DESC LIMIT 1")
            res2 = cursor.fetchall()
            y = res2[0][1]
            q = res2[0][2]

            d = datetime.date.today()
            current_q=0
            if(d.month<=3):
                current_q=1
            elif(d.month>3 and d.month<=6):
                current_q=2
            elif(d.month>6 and d.month<=9):
                current_q=3
            else:
                current_q=4

            if(y==d.year and q==current_q):
                sql0 = "UPDATE editions SET date_of_publication_on_site = %s , id_coef_of_formula = %s WHERE year=%s and quarter=%s"
                adr0 = (d, id, y, q)
                cursor.execute(sql0, adr0)
                db.commit()
            else:
                sql = "INSERT INTO editions (year, quarter, date_of_publication_on_site, id_coef_of_formula) VALUES (%s, %s, %s, %s)"
                val = (d.year, q, d, 1)
                cursor2.execute(sql, val)
                db.commit()

        with open(self.filename, "r") as f_obj:
            reader = csv.reader(f_obj)
            for row in reader:
                obj=Scientist(row)
                if(self.CheckTableOnExist('scientists', cursor)):
                    sql = "SELECT id FROM scientists WHERE name = %s and last_name = %s and middle_name = %s and academic_title = %s"
                    adr = (obj.name, obj.lname, obj.mname, obj.ac_title)
                    cursor.execute(sql, adr)
                    res = cursor.fetchall()
                    if(len(res)==0):
                        scientists.append(obj)
                else:
                    scientists.append(obj)
            if(self.CheckTableOnExist('scientists', cursor)==False):
                cursor2 = db.cursor()
                cursor2.execute("CREATE TABLE scientists (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), middle_name VARCHAR(255), last_name VARCHAR(255), academic_title VARCHAR(255), academic_degree VARCHAR(255),en_initials VARCHAR(255), en_last_name VARCHAR(255), id_scopus BIGINT)")
                db.commit()

            for value in scientists:
                cursor2 = db.cursor()
                sql = "INSERT INTO scientists (name, middle_name, last_name, academic_title, academic_degree, " \
                      "en_initials, en_last_name, id_scopus) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                val = (value.name, value.mname, value.lname, value.ac_title, value.ac_degree, value.en_initials, value.en_lname, value.id_scopus)
                cursor2.execute(sql, val)
                db.commit()

        if(self.CheckTableOnExist('publications', cursor)==False):
            cursor.execute(
                "CREATE TABLE sources (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), id_scopus BIGINT,"
                "url_scopus TEXT)")
            db.commit()
            cursor.execute(
                "CREATE TABLE publications (id INT AUTO_INCREMENT PRIMARY KEY, id_scopus VARCHAR(255), title TEXT,"
                "url_scopus TEXT, authors TEXT, citations INT, id_scientist INT, id_source INT, year INT, FOREIGN KEY(id_scientist) REFERENCES scientists(id), FOREIGN KEY(id_source) REFERENCES sources(id))")
            db.commit()


        cursor.execute("SELECT * FROM editions ORDER BY id DESC LIMIT 1")
        res = cursor.fetchall()
        id_edition=res[0]

        cursor.execute("SELECT * FROM scientists")
        result = cursor.fetchall()
        for value in result:
            publications=[]
            au_id_scopus = value[8]
            if au_id_scopus != None:
                au = AuthorRetrieval(au_id_scopus)
                publ_of_scientist = au.get_document_eids()
                for val in publ_of_scientist:
                    s=Source(val)
                    sql = "SELECT id FROM sources WHERE name = %s and id_scopus = %s"
                    adr = (s.name, s.id_scopus)
                    cursor.execute(sql, adr)
                    res = cursor.fetchall()
                    if(len(res)==0):
                        sql = "INSERT INTO sources (name, id_scopus, url_scopus) VALUES (%s, %s, %s)"
                        val1 = (s.name, s.id_scopus, s.url_scopus)
                        cursor.execute(sql, val1)
                        db.commit()
                    sql = "SELECT id FROM sources WHERE name = %s and id_scopus = %s"
                    adr = (s.name, s.id_scopus)
                    cursor.execute(sql, adr)
                    res = cursor.fetchall()
                    id_source=res[0]

                    sql2 = "SELECT id FROM scientists WHERE id_scopus = %s"
                    adr2 = []
                    adr2.append(au_id_scopus)
                    cursor.execute(sql2, adr2)
                    res2 = cursor.fetchall()

                    publ=Publication(val)
                    sql = "SELECT id FROM publications WHERE title = %s and id_scopus = %s and id_scientist= %s"
                    adr = (publ.title, publ.code, res2[0])
                    cursor.execute(sql, adr)
                    res = cursor.fetchall()
                    if (len(res) == 0):
                        sql = "INSERT INTO publications (id_scopus, title, url_scopus, authors, citations, id_scientist, id_source, year) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                        val = (publ.code, publ.title, publ.url_scopus, publ.authors, publ.citations, res2[0], id_source, publ.year)
                        cursor.execute(sql, val)
                        db.commit()

        if(self.CheckTableOnExist('sources_history', cursor)==False):
            cursor.execute(
                "CREATE TABLE sources_history (id INT AUTO_INCREMENT PRIMARY KEY, id_source INT, id_edition INT, snip  FLOAT, "
                "top  INT, quartile INT, percentile INT, FOREIGN KEY(id_source) REFERENCES sources(id), FOREIGN KEY(id_edition) REFERENCES editions(id))")
            db.commit()


        sql2 = "SELECT * FROM sources"
        cursor.execute(sql2)
        res2 = cursor.fetchall()
        i=1
        for value in res2:
            id=value[0]
            sql="SELECT id_scopus FROM publications WHERE id_source=%s LIMIT 1"
                #2-s2.0-
            adr2 = []
            adr2.append(id)
            cursor.execute(sql, adr2)
            res3 = cursor.fetchall()
            id_publ=str(res3[0][0])
            ab = ScopusAbstract(id_publ)
            snip = None
            percentile = None
            quartile = None
            top = None
            if (ab.issn != None):
                isr = ab.issn
                if (isr.find(' ') != -1):
                    isr = (isr.split(' '))[1]
                try:
                    r = ScopusJournal(isr)
                    snip = r.SNIP

                    pub_info = scopus1.retrieve_serial(isr)
                    if (pub_info[2].size != 0):
                        percentile = int((pub_info[2]['percentile'])[0])
                        if (percentile >= 75):
                            quartile = 1
                        elif (percentile < 75 and percentile >= 50):
                            quartile = 2
                        elif (percentile < 50 and percentile >= 25):
                            quartile = 3
                        else:
                            quartile = 4
                        if (percentile >= 90):
                            top = 10
                        if (quartile == 1 and percentile < 90):
                            top = 25
                except:
                    print('')

            sql = "SELECT id FROM sources_history WHERE id_edition = %s and id_source=%s"
            adr2 = (id_edition[0], id)
            cursor.execute(sql, adr2)
            res = cursor.fetchall()
            i+=1
            if (len(res) == 0):
                sql3 = "INSERT INTO sources_history(id_source, id_edition, snip, top, " \
                      "quartile, percentile) VALUES (%s, %s, %s, %s, %s, %s)"
                val3 = (id, id_edition[0], snip, top, quartile, percentile)
                cursor.execute(sql3, val3)
                db.commit()
            else:
                sql3="UPDATE sources_history SET snip = %s , top = %s , quartile=%s , percentile=%s WHERE id_source=%s and id_edition=%s"
                adr3=(snip, top, quartile, percentile, id, id_edition[0])
                cursor.execute(sql3, adr3)
                db.commit()

        if(self.CheckTableOnExist('rating_history', cursor)==False):
            cursor.execute(
                "CREATE TABLE rating_history (id INT AUTO_INCREMENT PRIMARY KEY, id_scientist INT, id_edition INT, count_of_publications INT, "
                "count_of_citations  INT, N_top10 INT, N_top25 INT, N_other INT, N_citations INT, rating INT, FOREIGN KEY(id_scientist) REFERENCES scientists(id), FOREIGN KEY(id_edition) REFERENCES editions(id))")
            db.commit()

        on_create=True
        sql = "SELECT id FROM rating_history WHERE id_edition = %s"
        adr2 = []
        adr2.append(id_edition[0])
        cursor.execute(sql, adr2)
        res = cursor.fetchall()
        if (len(res) != 0):
            on_create = False
        cursor.execute("SELECT id, id_scopus FROM scientists")
        result = cursor.fetchall()
        cursor.execute("SELECT * FROM parameters ORDER BY id DESC LIMIT 1")
        res = cursor.fetchall()
        time_win=res[0][5]
        d = datetime.date.today()
        year_from=d.year-time_win
        i=1
        for value in result:
            n_top10=0
            n_top25=0
            n_others=0
            n_citations=0
            citations=0
            docs=0
            sql2="SELECT id_source, id_scopus FROM publications WHERE id_scientist=%s and year>=%s"
            adr3 = (value[0], year_from)
            cursor.execute(sql2, adr3)
            result2 = cursor.fetchall()
            if len(result2)!=0:
                for value2 in result2:
                    sql3 = "SELECT * FROM sources_history WHERE id_source=%s"
                    adr4 = []
                    adr4.append(value2[0])
                    cursor.execute(sql3, adr4)
                    result3 = cursor.fetchall()
                    ab=ScopusAbstract(value2[1])
                    n_citations+=ab.citedby_count
                    snip=result3[0][3]
                    top=result3[0][4]
                    if top==10:
                        n_top10+=1
                        top=0
                    elif top==25:
                        n_top25+=1
                        top=0
                    elif snip!=None and snip>0:
                        n_others+=1
                try:
                    au = AuthorRetrieval(value[1])
                    citations = int(au.citation_count)
                    docs=int(au.document_count)
                except:
                    print('')
            i+=1

            R = res[0][1]*n_top10+res[0][2]*n_top25+res[0][3]*n_others+res[0][4]*n_citations
            if on_create:
                sql4 = "INSERT INTO rating_history(id_scientist, id_edition, count_of_publications, count_of_citations, N_top10, N_top25, N_other, N_citations, rating) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
                val4 = (value[0], id_edition[0], docs, citations, n_top10, n_top25, n_others, n_citations, R)
                cursor.execute(sql4, val4)
                db.commit()
            else:
                sql4="UPDATE rating_history SET count_of_publications = %s , count_of_citations = %s , N_top10=%s , " \
                     "N_top25=%s, N_other=%s, N_citations=%s, rating=%s WHERE id_scientist=%s and id_edition=%s"
                adr4=(docs, citations, n_top10, n_top25, n_others, n_citations, R, value[0], id_edition[0])
                cursor.execute(sql4, adr4)
                db.commit()

        drop="DROP PROCEDURE IF EXISTS view_rating"
        cursor.execute(drop)
        db.commit()

        dbproc="CREATE PROCEDURE view_rating() NO SQL BEGIN select scientists.id, scientists.last_name, scientists.name, " \
               "scientists.middle_name, scientists.academic_title, scientists.academic_degree, scientists.id_scopus, rating_history.N_top10, " \
               "rating_history.N_top25, rating_history.N_other, rating_history.N_citations, rating_history.rating from rating.scientists, " \
               "rating.rating_history where rating_history.id_scientist=scientists.id ORDER by rating_history.rating DESC, scientists.last_name ASC; END"
        cursor.execute(dbproc)
        db.commit()

        drop="DROP PROCEDURE IF EXISTS publications_top10_of_scientist"
        cursor.execute(drop)
        db.commit()
        dbproc="CREATE PROCEDURE publications_top10_of_scientist(IN id_s INT) NO SQL BEGIN declare d INT; declare w_t INT; declare ed INT; " \
               "SET d=YEAR(CURDATE()); SELECT distinct find_window_time() into w_t; SELECT distinct find_edition() into ed; " \
               "select publications.title, publications.url_scopus as publ_url, publications.authors, publications.year, publications.citations, sources.name as source_name, sources.url_scopus as source_url from publications, scientists, sources, sources_history where scientists.id=id_s and scientists.id=publications.id_scientist " \
               "and publications.year>=d-w_t and publications.id_source=sources.id and sources_history.id_source=sources.id and sources_history.id_edition=ed and sources_history.top=10 order by publications.title; END"
        cursor.execute(dbproc)
        db.commit()

        drop="DROP PROCEDURE IF EXISTS publications_top25_of_scientist"
        cursor.execute(drop)
        db.commit()
        dbproc="CREATE PROCEDURE publications_top25_of_scientist(IN id_s INT) NO SQL BEGIN declare d INT; declare w_t INT; declare ed INT; " \
               "SET d=YEAR(CURDATE()); SELECT distinct find_window_time() into w_t; SELECT distinct find_edition() into ed; " \
               "select publications.title, publications.url_scopus as publ_url, publications.authors, publications.year, publications.citations, sources.name as source_name, sources.url_scopus as source_url from publications, scientists, sources, sources_history where scientists.id=id_s and scientists.id=publications.id_scientist " \
               "and publications.year>=d-w_t and publications.id_source=sources.id and sources_history.id_source=sources.id and sources_history.id_edition=ed and sources_history.top=25 order by publications.title; END"
        cursor.execute(dbproc)
        db.commit()

        drop="DROP PROCEDURE IF EXISTS publications_others_of_scientist"
        cursor.execute(drop)
        db.commit()
        dbproc="CREATE PROCEDURE publications_others_of_scientist(IN id_s INT) NO SQL BEGIN declare d INT; declare w_t INT; declare ed INT; " \
               "SET d=YEAR(CURDATE()); SELECT distinct find_window_time() into w_t; SELECT distinct find_edition() into ed; " \
               "select publications.title, publications.url_scopus as publ_url, publications.authors, publications.year, publications.citations, sources.name as source_name, sources.url_scopus as source_url from publications, scientists, sources, sources_history where scientists.id=id_s and scientists.id=publications.id_scientist and " \
               "publications.year>=d-w_t and publications.id_source=sources.id and sources_history.id_source=sources.id and sources_history.id_edition=ed and sources_history.top is NULL and sources_history.snip>0 order by publications.title; END"
        cursor.execute(dbproc)
        db.commit()

        drop="DROP PROCEDURE IF EXISTS show_coefs"
        cursor.execute(drop)
        db.commit()
        dbproc="CREATE PROCEDURE show_coefs() NO SQL BEGIN SELECT * FROM parameters ORDER BY parameters.id DESC LIMIT 1; END"
        cursor.execute(dbproc)
        db.commit()

        drop="DROP PROCEDURE IF EXISTS find_date_of_create_rat"
        cursor.execute(drop)
        db.commit()
        dbproc="CREATE PROCEDURE find_date_of_create_rat() NO SQL BEGIN SELECT editions.date_of_publication_on_site FROM editions ORDER BY editions.id DESC LIMIT 1; END"
        cursor.execute(dbproc)
        db.commit()

        drop="DROP FUNCTION IF EXISTS find_edition"
        cursor.execute(drop)
        db.commit()
        dbproc="CREATE FUNCTION find_edition() RETURNS int(11) NO SQL BEGIN DECLARE val INTEGER; SELECT editions.id INTO val FROM editions " \
               "ORDER BY editions.id DESC LIMIT 1; return val; END"
        cursor.execute(dbproc)
        db.commit()

        drop="DROP FUNCTION IF EXISTS find_window_time"
        cursor.execute(drop)
        db.commit()
        dbproc="CREATE FUNCTION find_window_time() RETURNS int(11) NO SQL BEGIN DECLARE val INTEGER; SELECT parameters.time_window INTO val FROM parameters ORDER BY parameters.id " \
               "DESC LIMIT 1; return val; END"
        cursor.execute(dbproc)
        db.commit()

#obj=ScopusDataCollection("f1.csv", 1)
#obj.FillDB()

if __name__ == "__main__":
    if len(sys.argv) ==8:
        file = sys.argv[2]
        win_time=sys.argv[3]
        coef10=sys.argv[4]
        coef25=sys.argv[5]
        coef_others=sys.argv[6]
        coef_c=sys.argv[7]

        obj = ScopusDataCollection(file, win_time, coef10, coef25, coef_others, coef_c)
        obj.FillDB()
    elif len(sys.argv) ==3:
        file = sys.argv[2]

        obj = ScopusDataCollection(file)
        obj.FillDB()
        
    elif len(sys.argv) ==4:
        file = sys.argv[2]
        win_time=sys.argv[3]

        obj = ScopusDataCollection(file, win_time)
        obj.FillDB()
        
    elif len(sys.argv) ==2:
        print("Некорректные данные.")
        sys.exit(1)
